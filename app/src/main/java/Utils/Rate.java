package Utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.example.ehsan.androidrate.AppRate;
import com.example.ehsan.androidrate.OnClickButtonListener;
import com.example.ehsan.androidrate.R;


public class Rate{

    public static int installDays=10;
    public static int lunchTimes=5;
    public static int remindInterval=2;
    public static boolean showNeeutralButton=true;

    public static void show(Context context){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_rate, null);
        AppRate.with(context)
                .setInstallDays(installDays) // default 10, 0 means install day.
                .setLaunchTimes(lunchTimes) // default 10 times.
                .setRemindInterval(remindInterval) // default 1 day.
                .setShowNeutralButton(showNeeutralButton) // default true.
                .setDebug(true) // default false.
                .setOnClickButtonListener(new OnClickButtonListener() { // callback listener.
                    @Override
                    public void onClickButton(int which) {
                        // Log.d(MainActivity.class.getName(), Integer.toString(which));
                    }
                }).setView(view)
                .monitor();
        AppRate.showRateDialogIfMeetsConditions((Activity) context);
    }
}
