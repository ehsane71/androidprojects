package com.example.ehsan.androidrate;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.View;
import android.widget.Button;

import static com.example.ehsan.androidrate.IntentHelper.createIntentForGooglePlay;
import static com.example.ehsan.androidrate.PreferenceHelper.setAgreeShowDialog;
import static com.example.ehsan.androidrate.PreferenceHelper.setRemindInterval;
import static com.example.ehsan.androidrate.Utils.getDialogBuilder;

final class DialogManager {

    private DialogManager() {
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    static Dialog create(final Context context, boolean isShowNeutralButton,
                         boolean isShowTitle, final OnClickButtonListener listener, View view) {
        AlertDialog.Builder builder = getDialogBuilder(context);
        //builder.setMessage(R.string.rate_dialog_message);
        //if (isShowTitle) builder.setTitle(R.string.rate_dialog_title);
        if (view != null) builder.setView(view);
        Button okButton = (Button) ((view).findViewById(R.id.dialog_ok_button));
        okButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                context.startActivity(createIntentForGooglePlay(context));
                setAgreeShowDialog(context, false);
                if (listener != null) listener.onClickButton(1);
            }
        });
        Button noButton = (Button) view.findViewById(R.id.dialog_no_button);
        noButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                setAgreeShowDialog(context, false);
                if (listener != null) listener.onClickButton(2);
            }
        });
        Button cancelButton = (Button) view.findViewById(R.id.dialog_cancel_button);
        if (isShowNeutralButton) {
            cancelButton.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    setRemindInterval(context);
                    if (listener != null) listener.onClickButton(3);
                }
            });
        } else {
            cancelButton.setVisibility(View.GONE);
        }
        /*builder.setPositiveButton(R.string.rate_dialog_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.startActivity(createIntentForGooglePlay(context));
                setAgreeShowDialog(context, false);
                if (listener != null) listener.onClickButton(which);
            }
        });
        if (isShowNeutralButton) {
            builder.setNeutralButton(R.string.rate_dialog_cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setRemindInterval(context);
                    if (listener != null) listener.onClickButton(which);
                }
            });
        }
        builder.setNegativeButton(R.string.rate_dialog_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setAgreeShowDialog(context, false);
                if (listener != null) listener.onClickButton(which);
            }
        });*/
        return builder.create();
    }


}
