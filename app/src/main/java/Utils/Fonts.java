package Utils;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.widget.TextView;

public abstract class Fonts {

	public static final String ROBOTO_THIN = "Roboto-Thin.ttf";
	public static final String ROBOTO_LIGHT = "Roboto-Light.ttf";
	public static final String ROBOTO_MEDIUM = "Roboto-Medium.ttf";
	public static final String ROBOTO_REGULAR = "Roboto-Regular.ttf";
	public static final String YEKAN = "Yekan.ttf";

	private static Typeface ROBOTO_REGULAR_FONT;
	private static Typeface ROBOTO_LIGHT_FONT;
	private static Typeface YEKAN_FONT;

	public static void setTypeFace(TextView tv, String font) {
		tv.setTypeface(getTypeFaceFromAssets(tv.getContext(), font));
	}

	public static Typeface getTypeFaceFromAssets(Context context,
			String fontName) {
		return Typeface.createFromAsset(context.getAssets(), "fonts/"
				+ fontName);
	}
	
	public static Typeface getRobotoRegular(Context context) {
		if (ROBOTO_REGULAR_FONT == null) {
			ROBOTO_REGULAR_FONT = getTypeFaceFromAssets(context, ROBOTO_REGULAR);
		}
		return ROBOTO_REGULAR_FONT;
	}

	public static Typeface getRobotoLight(Context context) {
		if (ROBOTO_LIGHT_FONT == null) {
			ROBOTO_LIGHT_FONT = getTypeFaceFromAssets(context, ROBOTO_LIGHT);
		}
		return ROBOTO_LIGHT_FONT;
	}

	public static Typeface getYekan(Context context) {
		if (YEKAN_FONT == null) {
			YEKAN_FONT = getTypeFaceFromAssets(context, YEKAN);
		}
		return YEKAN_FONT;
	}

	public static void setTextViewFontSizeByScreenSize(TextView textView,
			int large_size_sp, int medium_size_sp, int small_size_sp,
			int default_size_sp) {

		int screenSize = textView.getContext().getResources()
				.getConfiguration().screenLayout
				& Configuration.SCREENLAYOUT_SIZE_MASK;

		switch (screenSize) {
		case Configuration.SCREENLAYOUT_SIZE_LARGE:
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, large_size_sp);
			break;
		case Configuration.SCREENLAYOUT_SIZE_NORMAL:
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, medium_size_sp);
			break;
		case Configuration.SCREENLAYOUT_SIZE_SMALL:
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, small_size_sp);
			break;
		default:
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, default_size_sp);
		}
	}

}
