package CustomViews;

import android.util.TypedValue;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import Utils.Fonts;


public class FarsiButton extends Button {

	public FarsiButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public FarsiButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public FarsiButton(Context context) {
		super(context);
		init();
	}

	private void init() {
		if (!isInEditMode()) {
			setTypeface(Fonts.getYekan(getContext()));
			float textSize = getTextSize();
			setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize * 1.2f); 
			setPadding(getPaddingLeft(), (int) (-textSize/3.6f), getPaddingRight(), (int) (-textSize/1.7f));
		}
	}

}
