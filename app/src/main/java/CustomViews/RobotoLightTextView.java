package CustomViews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import Utils.Fonts;

public class RobotoLightTextView extends TextView {

	public RobotoLightTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public RobotoLightTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public RobotoLightTextView(Context context) {
		super(context);
		init();
	}

	private void init() {
		if(!isInEditMode()){
			setTypeface(Fonts.getTypeFaceFromAssets(getContext(), Fonts.ROBOTO_LIGHT));
		}
	}
	
}
