package CustomViews;

import android.util.TypedValue;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;



import Utils.Fonts;
import Utils.Language;


public class UniqueTextView extends TextView {

	public UniqueTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
//		applyAttributes(attrs,context);
	}

	public UniqueTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
//		applyAttributes(attrs,context);
	}

	public UniqueTextView(Context context) {
		super(context);
		init(context);
	}

	private void init(Context context) {
		if (isInEditMode()) return;
		
		if (Language.isRTL()) {
			this.setTypeface(Fonts.getYekan(context));
		} else {			
			this.setTypeface(Fonts.getRobotoRegular(context));
			this.setTextSize(TypedValue.COMPLEX_UNIT_PX, this.getTextSize() * 0.9f);
		}
	}
	
	/*private void applyAttributes(AttributeSet attrs, Context context) {
		TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
				R.styleable.UniqueTextView, 0, 0);
		try {
			switch (a.getInt(R.styleable.UniqueTextView_font, -1)) {
			case 0:
				this.setTypeface(Fonts.getTypeFaceFromAssets(getContext(), Fonts.ROBOTO_LIGHT));
				break;
			case 1:
				this.setTypeface(Fonts.getTypeFaceFromAssets(getContext(), Fonts.ROBOTO_MEDIUM));
				break;
			case 2:
				this.setTypeface(Fonts.getTypeFaceFromAssets(getContext(), Fonts.ROBOTO_REGULAR));
				break;
			case 3:
				this.setTypeface(Fonts.getTypeFaceFromAssets(getContext(), Fonts.ROBOTO_THIN));
				break;
			default:
				break;
			}
		} finally {
			a.recycle();
		}
	}*/
}
