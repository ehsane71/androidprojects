package Utils;

import android.content.Context;
import android.content.res.Configuration;


import com.example.ehsan.androidrate.R;



import java.util.Locale;

public abstract class Language {

    public static final String FARSI = "fa";
    public static final String ENGLISH = "en";
    public static final String TAG = Language.class.getSimpleName();

    public static boolean isPersian() {
        return FARSI.equals(Locale.getDefault().getLanguage());
    }

    public static void changeToPersian(Context context) {
        changeTo(FARSI, context);
    }

    public static void changeToEnglish(Context context) {
        changeTo(ENGLISH, context);
    }

    public static void changeTo(String language, Context context) {

        if (!isLanguageSupported(language, context)) {
            language = ENGLISH;
        }

        String country = "";
        String languageToLoad = "";

        if (language.contains("_")) {
            try {
                country = language.split("_")[1];
                languageToLoad = language.split("_")[0];
            } catch (Exception e) {
               // if (Debug.LOG) Log.e(TAG, "Bad entry value for language : " + language);
            }
        }
        else {
            languageToLoad = language;
        }

        //if (Debug.LOG)
          //  Log.i(TAG, "change Locale : Language = " + languageToLoad + ", country = " + country);

        Locale locale = new Locale(languageToLoad, country);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
        //Settings.setLanguage(language, context);
    }

    public static boolean isLanguageSupported(String language, Context context) {
        String[] langs = getLanguagesSupported(context);
        for (int i = 0; i < langs.length; i++) {
            if (langs[i].equals(language))
                return true;
        }
        return false;
    }

    public static String[] getLanguagesSupported(Context context) {
        return context.getResources().getStringArray(R.array.values_language);
    }

    public static boolean isRTL() {
        return isRTL(Locale.getDefault());
    }

    public static boolean isRTL(Locale locale) {
        final int directionality = Character.getDirectionality(locale.getDisplayName().charAt(0));
        return directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT ||
                directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC;
    }
}